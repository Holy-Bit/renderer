var hierarchy =
[
    [ "renderer::Color_Buffer_Rgba8888::Color", "structrenderer_1_1_color___buffer___rgba8888_1_1_color.html", null ],
    [ "renderer::Color_Buffer_Rgb565::Color", "structrenderer_1_1_color___buffer___rgb565_1_1_color.html", null ],
    [ "renderer::Color_Buffer", "classrenderer_1_1_color___buffer.html", [
      [ "renderer::Color_Buffer_Rgb565", "classrenderer_1_1_color___buffer___rgb565.html", null ],
      [ "renderer::Color_Buffer_Rgba8888", "classrenderer_1_1_color___buffer___rgba8888.html", null ]
    ] ],
    [ "renderer::Mesh", "classrenderer_1_1_mesh.html", null ],
    [ "renderer::Model", "classrenderer_1_1_model.html", [
      [ "renderer::Obj_Loader", "classrenderer_1_1_obj___loader.html", null ]
    ] ],
    [ "renderer::Model_Obj", "classrenderer_1_1_model___obj.html", null ],
    [ "renderer::Rasterizer< COLOR_BUFFER_TYPE >", "classrenderer_1_1_rasterizer.html", null ],
    [ "renderer::Rasterizer< renderer::Color_Buffer_Rgba8888 >", "classrenderer_1_1_rasterizer.html", null ],
    [ "renderer::Model::Transform", "structrenderer_1_1_model_1_1_transform.html", null ],
    [ "renderer::View", "classrenderer_1_1_view.html", null ]
];