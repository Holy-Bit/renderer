var classrenderer_1_1_mesh =
[
    [ "Color", "classrenderer_1_1_mesh.html#a71276bd4511d4593f1deaae6d464e811", null ],
    [ "Color_Buffer", "classrenderer_1_1_mesh.html#aeb2cb93e67b18f09a15e7ece644f919b", null ],
    [ "Index_Buffer", "classrenderer_1_1_mesh.html#a6a226cb85370a00787fe01f70a0b5ca2", null ],
    [ "Vertex", "classrenderer_1_1_mesh.html#ab0cb2995275ebaa8ae9e5fbb5519b7f3", null ],
    [ "Vertex_Buffer", "classrenderer_1_1_mesh.html#a8c848a95f65fb25f6dc94dcb8879cf8b", null ],
    [ "Vertex_BufferI", "classrenderer_1_1_mesh.html#ac5074bb370a7062d07e11150de94e8be", null ],
    [ "Vertex_Colors", "classrenderer_1_1_mesh.html#ab384ccca7ccccac4b935fe797ce0a7ea", null ],
    [ "VertexI", "classrenderer_1_1_mesh.html#a5fd87426b3c8820d68414531423446fc", null ],
    [ "Mesh", "classrenderer_1_1_mesh.html#a9599aef3cd7b6e8b4643a408445f272b", null ],
    [ "is_frontface", "classrenderer_1_1_mesh.html#a693d10fdfac269136fe703a53a7cc27e", null ],
    [ "render", "classrenderer_1_1_mesh.html#a09c57a296515d4db731b2aa0f4eba284", null ],
    [ "set_index", "classrenderer_1_1_mesh.html#aa2e7ea68a34e4f2f5853136c1efcff7a", null ],
    [ "set_Transform", "classrenderer_1_1_mesh.html#acaed062c3d1abf115f22f7dba87ed69c", null ],
    [ "set_Vertex", "classrenderer_1_1_mesh.html#a6d48b23fa3c6fd467e15fdd4f12a6f5a", null ],
    [ "update", "classrenderer_1_1_mesh.html#a794be273829e332a1ba81d9f5af3b60e", null ],
    [ "clipped_vertices", "classrenderer_1_1_mesh.html#a895006513e43820bed66a13e6860c072", null ],
    [ "display_vertices", "classrenderer_1_1_mesh.html#ab375734fbd3ad209425e38f89f64ab5d", null ],
    [ "index_positions", "classrenderer_1_1_mesh.html#a67f50b2b651091b38567c8fd69e9e4b3", null ],
    [ "light_colors", "classrenderer_1_1_mesh.html#a548a365c33a70a93e8a1f7c4085b1ec8", null ],
    [ "light_vector", "classrenderer_1_1_mesh.html#a6bf2d3288700befda4d24c6ee44e7a3f", null ],
    [ "normals", "classrenderer_1_1_mesh.html#a88ff332e89b6278946fc7f61f0014d0d", null ],
    [ "original_colors", "classrenderer_1_1_mesh.html#aa32dd73ecaaaf1f5594f12c6208df116", null ],
    [ "transform", "classrenderer_1_1_mesh.html#a6d4a64b911980ccd50a4fc54830ce653", null ],
    [ "transformed_normals", "classrenderer_1_1_mesh.html#a17a0e6c0c5d85b761ae43ec601a346ea", null ],
    [ "transformed_vertices", "classrenderer_1_1_mesh.html#a670137619aeda905f64bc784265ccbe5", null ],
    [ "vertexs", "classrenderer_1_1_mesh.html#a09d0844ff04aec6c504b44c261cf81e1", null ]
];