var classrenderer_1_1_color___buffer =
[
    [ "Color_Buffer", "classrenderer_1_1_color___buffer.html#a42e228a4ed403c292b22fe69e4f9e092", null ],
    [ "bits_per_color", "classrenderer_1_1_color___buffer.html#ac03bd34681ed79f8b36c9c7292db98e4", null ],
    [ "get_height", "classrenderer_1_1_color___buffer.html#a75e5eddc9bc168ee6c65276b1e716047", null ],
    [ "get_width", "classrenderer_1_1_color___buffer.html#a719d5481887bcae92797f566b21d2b64", null ],
    [ "gl_draw_pixels", "classrenderer_1_1_color___buffer.html#a1108142247a2814d028b5ffbcde72d52", null ],
    [ "offset_at", "classrenderer_1_1_color___buffer.html#a899ad44fd4d44ffed9e0704f97e01daf", null ],
    [ "set_color", "classrenderer_1_1_color___buffer.html#ac99a897d15211d3754e343928412b765", null ],
    [ "set_pixel", "classrenderer_1_1_color___buffer.html#a383c851cb75200d8924857b1b28a675e", null ],
    [ "set_pixel", "classrenderer_1_1_color___buffer.html#a02e8a2b93f5d95f10e2aedd24ee02625", null ],
    [ "height", "classrenderer_1_1_color___buffer.html#a9a26f6f8e04447ac2b26c38e0a190714", null ],
    [ "width", "classrenderer_1_1_color___buffer.html#ace3fab0552b94a1d7d7afcc592cd4503", null ]
];