var classrenderer_1_1_color___buffer___rgb565 =
[
    [ "Color", "structrenderer_1_1_color___buffer___rgb565_1_1_color.html", "structrenderer_1_1_color___buffer___rgb565_1_1_color" ],
    [ "Buffer", "classrenderer_1_1_color___buffer___rgb565.html#a60feae2b617049e32ecfb9e87349272c", null ],
    [ "Color_Buffer_Rgb565", "classrenderer_1_1_color___buffer___rgb565.html#ad08f0151e303070965a5c4624577a823", null ],
    [ "bits_per_color", "classrenderer_1_1_color___buffer___rgb565.html#a71920b761aa6ce5b05c1e587c168a6d4", null ],
    [ "colors", "classrenderer_1_1_color___buffer___rgb565.html#aa209340b48cd679650f5965caef8dd76", null ],
    [ "colors", "classrenderer_1_1_color___buffer___rgb565.html#acf5016c64a12241e770a149dc1793fce", null ],
    [ "gl_draw_pixels", "classrenderer_1_1_color___buffer___rgb565.html#a8e9e5e0646c4ac90e4867e8f83352eaa", null ],
    [ "set_color", "classrenderer_1_1_color___buffer___rgb565.html#aab74559163ff67f50416f5dc5a85bc97", null ],
    [ "set_color", "classrenderer_1_1_color___buffer___rgb565.html#aad9ed69a24b11f14f7efb6fc81bd82e7", null ],
    [ "set_pixel", "classrenderer_1_1_color___buffer___rgb565.html#ab3f0706720c48f42cbc8aa771ea3178e", null ],
    [ "set_pixel", "classrenderer_1_1_color___buffer___rgb565.html#af07fc1066c1654ed887952db87a8869a", null ],
    [ "size", "classrenderer_1_1_color___buffer___rgb565.html#a0bb8eba0bf02fdc13fe8e1c67906631e", null ],
    [ "buffer", "classrenderer_1_1_color___buffer___rgb565.html#af8e027cd7b139e5f795001db42c84058", null ],
    [ "color", "classrenderer_1_1_color___buffer___rgb565.html#a57d65a5c62338338f7081a32c9227e98", null ]
];