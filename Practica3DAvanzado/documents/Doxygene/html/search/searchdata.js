var indexSectionsWithContent =
{
  0: "abcdefghilmnoprstuvwz",
  1: "cmortv",
  2: "r",
  3: "cmorv",
  4: "bcfgimoprsuv",
  5: "abcdeghilmnoprstvwz",
  6: "bciv",
  7: "mot"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "typedefs",
  7: "defines"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Typedefs",
  7: "Macros"
};

