var searchData=
[
  ['g',['g',['../structrenderer_1_1_color___buffer___rgba8888_1_1_color.html#ac305740da991095868487b26e0effbf8',1,'renderer::Color_Buffer_Rgba8888::Color']]],
  ['get_5fcolor_5fbuffer',['get_color_buffer',['../classrenderer_1_1_rasterizer.html#aad1766bb1cf80e143aa3fefe7a3a52ff',1,'renderer::Rasterizer']]],
  ['get_5ferror',['get_error',['../classrenderer_1_1_obj___loader.html#a66ea56ee2d007bd9fc962889316b831c',1,'renderer::Obj_Loader']]],
  ['get_5fheight',['get_height',['../classrenderer_1_1_color___buffer.html#a75e5eddc9bc168ee6c65276b1e716047',1,'renderer::Color_Buffer']]],
  ['get_5fmeshes',['get_meshes',['../classrenderer_1_1_model.html#a88df819cc280f518023364c9181aff64',1,'renderer::Model']]],
  ['get_5fwidth',['get_width',['../classrenderer_1_1_color___buffer.html#a719d5481887bcae92797f566b21d2b64',1,'renderer::Color_Buffer']]],
  ['getmodel',['getModel',['../classrenderer_1_1_model.html#a2e81d98304670f47465b18010ec342cb',1,'renderer::Model']]],
  ['gl_5fdraw_5fpixels',['gl_draw_pixels',['../classrenderer_1_1_color___buffer.html#a1108142247a2814d028b5ffbcde72d52',1,'renderer::Color_Buffer::gl_draw_pixels()'],['../classrenderer_1_1_color___buffer___rgb565.html#a8e9e5e0646c4ac90e4867e8f83352eaa',1,'renderer::Color_Buffer_Rgb565::gl_draw_pixels()'],['../classrenderer_1_1_color___buffer___rgba8888.html#afef366739856f8d429cee6ced277d167',1,'renderer::Color_Buffer_Rgba8888::gl_draw_pixels()']]]
];
