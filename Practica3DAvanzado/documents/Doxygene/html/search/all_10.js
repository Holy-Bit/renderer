var searchData=
[
  ['tinyobjloader_5fimplementation',['TINYOBJLOADER_IMPLEMENTATION',['../_obj___loader_8cpp.html#af14fac7fbc250522a78849d58d5b0811',1,'Obj_Loader.cpp']]],
  ['transform',['Transform',['../structrenderer_1_1_model_1_1_transform.html',1,'renderer::Model::Transform'],['../classrenderer_1_1_mesh.html#a6d4a64b911980ccd50a4fc54830ce653',1,'renderer::Mesh::transform()'],['../classrenderer_1_1_model.html#a9689817daed05747058d3b919af724fd',1,'renderer::Model::transform()']]],
  ['transformation',['transformation',['../structrenderer_1_1_model_1_1_transform.html#a23b32f9024b97b529d1febaec190e997',1,'renderer::Model::Transform']]],
  ['transformed_5fnormals',['transformed_normals',['../classrenderer_1_1_mesh.html#a17a0e6c0c5d85b761ae43ec601a346ea',1,'renderer::Mesh']]],
  ['transformed_5fvertices',['transformed_vertices',['../classrenderer_1_1_mesh.html#a670137619aeda905f64bc784265ccbe5',1,'renderer::Mesh']]],
  ['translation',['translation',['../structrenderer_1_1_model_1_1_transform.html#ad2274abae9bed064000673eb37d3f742',1,'renderer::Model::Transform']]]
];
